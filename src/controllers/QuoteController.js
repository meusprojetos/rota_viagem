const output = require('../models/Output');
const Quote = require('../models/Quote');

exports.consultingPrice = async (req, res) => {
   let QuoteInstance = new Quote();
   
   if (!req.params.from || !req.params.to) {
      output.responseError("Origin and destiny is required."); 
      return;  
   }

   let from = req.params.from;
   let to = req.params.to;
   
   let data = await QuoteInstance.consultBestPrice(from, to);

   output.responseJson(data, res);
};

