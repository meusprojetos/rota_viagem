const output = require('../models/Output');
const Route = require('../models/Route');

exports.createItinerary = async (req, res) => {
    let RouteInstance = new Route();
    let data = req.body;
 
    try {
       let insertData = await RouteInstance.insertRouter(data)
    } catch(err) {
       output.reponseError(err.message, res, 400);
    }
 
    output.responseJson({"message":  "Insert success."}, res);
 };
 