const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const quoteRoutes = require('./routes/Quote');
const routeRoutes = require('./routes/Route');

app.use(bodyParser.json());

app.get('/', (req, res) => {
   res.send('Welcome to test.')
})

app.use('/route', routeRoutes);
app.use('/quote', quoteRoutes);

module.exports = app;