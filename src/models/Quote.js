const Calculate = require('./Quote/Calculate');

class Quote
{
   /**
    * Return best price
    * 
    * @param {*} from 
    * @param {*} to 
    */
   async consultBestPrice(from, to)
   {
      let calculate = new Calculate(from, to);

      let bestPrice = calculate.bestPrice();

      return bestPrice;
   }

   
}

module.exports = Quote;