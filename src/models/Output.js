class Output
{
   static responseJson(data, res){
       res.type('json');
       res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
       res.status(200).json(data);
   }

   static reponseError(message, res, status){
       res.type('json');
       res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
       res.status(status).json({"success": false, "error": {
           "message": message
       }});
   }
}

module.exports = Output;