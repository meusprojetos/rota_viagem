const Route = require('../Route');

class Calculate
{
    constructor(from, to)
    {
        this.from = from;
        this.to = to;

        this.manipuleFrom = this.from;

        this.itinerary = new Array();
        this.range = new Array();
        this.ranges = new Array();
    }

    /**
     * @returns Route
     */
    getRoute()
    {
        return new Route();
    }

    /**
     * 
     */
    async bestPrice()
    {
        let possibilities = await this.genereteBestsRoutes();
        return await this.getBestPrice(possibilities);
    }

    /**
     * Return best route
     */
    async genereteBestsRoutes()
    {
        let itinerary = await this.colectItinerary(this.from);
        
        let ranges = await this.generateRange();
        let data = new Array();
        
        for (let range in ranges) {
            data.push(await this.calculatePrice(itinerary, ranges[range]));
        }
        
        return data;
    }

    /**
     * Return Itinerary
     * 
     * @param {*} destiny 
     * @param {*} recursive 
     */
    async colectItinerary(destiny, recursive = false)
    {
        if (destiny) {
            let filters = {'field': 'from', 'value':  destiny};
            
            let routes = await this.getRoute().getData(filters);
            
            this.itinerary[destiny] = new Array();

            for (let x in routes) {
                this.itinerary[destiny].push(routes[x]);
            }

            if (recursive) {
                return;
            }
        }

        let possibilitiesOrigin = await this.getRoute().getData();

        for (let x in possibilitiesOrigin) {
            await this.colectItinerary(possibilitiesOrigin[x].to, true);
        }

        return this.itinerary;
    }

    /**
     * Generate all possibles
     * 
     * @param {*} recursive 
     */
    async generateRange(recursive = false)
    {
        if (recursive && this.manipuleFrom == this.from) {
            return;
        }

        let originOptions = await this.getRoute().getData({
            'field': 'from', 
            'value':  this.manipuleFrom
        });
        
        if (!originOptions.length) {
            return;
        }

        this.range.push(this.manipuleFrom);
        
        if (this.manipuleFrom == this.to) {
            if (this.ranges.indexOf(this.range.join()) > -1) {
                this.range = new Array();
                return;
            }

            this.ranges.push(this.range.join());
            
            return;
        }
        
        for (let boarding in originOptions) {
            if (!recursive) {
                this.range = new Array();
                this.range.push(this.from);
            }
            
            if (this.ranges.indexOf(this.range.join()) > -1) {
                this.range = new Array();
                continue;
            }

            let range = new Array(); 
            for (let x in this.range) {
                if (this.range[x] == originOptions[boarding].from) {
                    range.push(this.range[x])
                    this.range = range;

                    break;
                }

                range.push(this.range[x])
            }

            if (this.to == originOptions[boarding].to) {
                if(!this.range.length) {
                    this.range = new Array();
                    continue;
                }

                this.range.push(originOptions[boarding].to);

                if (this.ranges.indexOf(this.range.join()) > -1) {
                    this.range = new Array();
                    continue;
                }
                
                if (this.range[0] != this.from) {
                    this.range = new Array();
                    continue;
                }


                this.ranges.push(this.range.join());
                
                this.range = new Array();
                continue;
            }

            this.manipuleFrom = originOptions[boarding].to;
            
            await this.generateRange(true);
        }

        return this.ranges;
    }

    /**
     * Calcule price all routes
     * 
     * @param {*} itinerary 
     * @param {*} range 
     */
    async calculatePrice(itinerary, range)
    {
        range = range.split(',');
        let price = 0;
        
        for (let x in range) {
            
            let next = range[parseInt(x) + 1];
            if (typeof next === 'undefined') {
                continue;
            }
            
            let value = await this.getPriceRoute(itinerary[range[x]], next);
            price = parseInt(price) + parseInt(value);
        }

        let routeFormat = {
            'route': range.join(),
            'price': price
        }

        return routeFormat;
    }

    /**
     * 
     * @param {*} itinerary 
     * @param {*} to 
     */
    getPriceRoute(itinerary, to)
    {
        for (let x in itinerary) {
            if (itinerary[x].to == to) {
                let value = parseInt(itinerary[x].price);
                
                return value;
            }
            continue;
        }

        return 0;
    }

    /**
     * Return min price
     * 
     * @param {*} routes 
     */
    getBestPrice(routes)
    {
        return routes.reduce((current, route) => route.price < current.price ? route : current, routes[0]);
    }

}

module.exports = Calculate;