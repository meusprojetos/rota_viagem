class Validator
{
    constructor()
    {
        this.Error = null;
    }

    /**
     * 
     * @param {*} data 
     */
    isValid(data)
    {
        if (!this.hasFrom(data)) {
            return false;
        }

        if (!this.hasTo(data)) {
            return false;
        }

        if (!this.hasPrice(data)) {
            return false;
        }

        return true;
    }

    /**
     * 
     * @param {*} data 
     */
    hasFrom(data)
    {
        if (typeof data.from === 'undefined' || !data.from) {
            this.Error = "Place of origin is required."
            return false;
        }

        return true;
    }

    /**
     * 
     * @param {*} data 
     */
    hasTo(data)
    {
        if (typeof data.from === 'undefined' || !data.to) {
            this.Error = "Destination is required."
            return false;
        }
        
        return true;
    }

    /**
     * 
     * @param {*} data 
     */
    hasPrice(data)
    {
        if (typeof data.from === 'undefined' || !data.price) {
            this.Error = "Price is required."
            return false;
        }
        
        return true;
    }
}

module.exports = Validator;