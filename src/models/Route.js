const DatabaseAbstract = require('./DatabaseAbstract');
const Validator = require('./Quote/Validator');

class Route
{
    constructor()
    {
        this.validator = new Validator();
    }

    /**
     * Return CSV Abstract
     * 
     */
    getDB()
    {
        return new DatabaseAbstract();
    }

    /**
     * 
     * @param {*} filters 
     */
    async getData(filters = null)
    {
        let data = await this.getDB().getData();
        
        if (filters) {
            let filterData = new Array();
            
            for(let x in data) {
                if (data[x][filters.field] == filters.value) {
                    filterData.push(data[x]);
                }
            }

            data = filterData;
        }
        
        return data;
    }

    /**
    * Insert new route
    * 
    * @param {*} data 
    */
    insertRouter(data)
    {
        if (!this.validator.isValid(data)) {
            throw new Error(this.validator.Error);
        }

        let values = [data.from, data.to, data.price];

        this.getDB().save(values.join());
    }

    /**
     * Truncate file CSV
     */
    truncate()
    {
        return this.getDB().truncate();
    }
}

module.exports = Route;