const fs = require('fs');

class DatabaseAbstract
{
   constructor()
   {
      this.file = 'database/routes.csv';
   }

   /**
    * Return values file
    */
   getData()
   {
      return new Promise( (resolve, reject) => {
         fs.readFile(this.file, 'utf8', (err, data) => {
            if (err) reject(err);

            let items = data.split("\n");

             resolve(this.normalizeData(items));
         });
       });
   }

   /**
    * Normalize values to json
    * 
    * @param {*} data 
    */
   normalizeData(data)
   {
      let normalize = new Array();

      for (let x in data) {
         if (!data[x]) {
            continue;
         }

         let item = data[x].split(",");

         let itemFormat = {
            'from': item[0],
            'to': item[1],
            'price': item[2]
         }

         normalize.push(itemFormat);
      }
      
      return normalize;
   }

   /**
    * Save in file
    * 
    * @param {*} data 
    */
   save(data)
   {
      return new Promise((resolve, reject) => {
         fs.appendFile(this.file, data + "\n", (err) => {
            if (err) console.error('Couldn\'t append the data');

            resolve('The data was appended to file.');
          });
      });
   }

   /**
    * remove all values in file
    */
   truncate() {
      fs.truncate(this.file, 0, function(){})
   }
}

module.exports = DatabaseAbstract;