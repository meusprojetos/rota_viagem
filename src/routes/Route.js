const express = require('express');
const router = express.Router();

const routeController = require('../controllers/RouteController');

router.post('/', routeController.createItinerary);

module.exports = router;
