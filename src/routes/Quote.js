const express = require('express');
const router = express.Router();

const quoteController = require('../controllers/QuoteController');

router.get('/:from/:to', quoteController.consultingPrice);

module.exports = router;
