const readline = require('readline');
const fs = require('fs');
const path = require('path');

path.extname('index.html') 
const quote = require('./models/Quote');
const route = require('./models/Route');

exports.console = () => {
    if (typeof process.argv[2] === 'undefined') {
        console.log('file was not informed.')
        return;
    }
     
    const quoteInstance = new quote();
    const routeInstance = new route();
    
    routeInstance.truncate();
    
    fs.readFile(process.argv[2], 'utf8', (err, data) => {
        if (err) console.log(err);
    
        let items = data.split("\n");
        for(let x in items) {
        let item = items[x].split(',');
        let data = {
            'from': item[0],
            'to': item[1],
            'price': item[2]
        };
    
        routeInstance.insertRouter(data);
        }
        
    });
    
    const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: 'please enter the route: '
    });
    
    rl.prompt();
    
    rl.on('line', async (line) => {
        let params = line.split('-');
        
        let data = await quoteInstance.consultBestPrice(params[0], params[1])
        
        if (data) {
        console.log('best route: ' + data.route + ' > ' +  '$' + data.price);
        console.log('\n');
        } else {
        console.log('invalid routes')
        }
    
    rl.prompt();
    }).on('close', () => {
        console.log('\n')
        console.log('thank you!');
        process.exit(0);
    });
 
}

 