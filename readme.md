# Teste - BEXS

Este programa de rotas de viagem, foi desenvolvido utilizando a linguagem de programação NodeJS.
### Requisitos

 - node js
 - npm 
 
[Manual de instalação](https://www.npmjs.com/get-npm)
 
### Instalação

Utilizando um terminal, acesse a pasta do projeto e execute o seguinte comando:

    npm install

### Execução

Para inicializar o programa, execute o comando:

    node server.js <path file csv>
 
*obs.: o programa cria um servidor http (http://localhost:3000)*

### Estrutura de arquivos

    /__tests__
    /database
	    routes.csv
    /src
	    /constrollers
	    /models
	    /routes
	    app.js
	    console.js
	server.js
	package.json

### API Rest

Endpoints:

 - /route POST (cria uma nova rota)
	 - `POST /route HTTP/1.1
Host: localhost:3000
Content-Type: application/json
{
	"from": "BRC",
	"to": "BA",
	"price": 10
}`
 - /quote/:from/:to GET (consultar melhor rota)
 - 
	 - `GET /quote/GRU/CDG HTTP/1.1
Host: localhost:3000`

### Testes unitários
Para executar os testes unitarios execute o seguinte comando:

    npm test
