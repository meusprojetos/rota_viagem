const DatabaseAbstract = require('../src/models/DatabaseAbstract');
const Calculate        = require('../src/models/Quote/Calculate');

const mockDatabaseReturn = [
    { from: 'GRU', to: 'BRC', price: '10' },
    { from: 'BRC', to: 'SCL', price: '5' },
    { from: 'GRU', to: 'SCL', price: '20' },
    { from: 'GRU', to: 'ORL', price: '56' },
    { from: 'ORL', to: 'CDG', price: '5' },
    { from: 'SCL', to: 'ORL', price: '20' },
    { from: 'GRU', to: 'CDG', price: '75' }
];

jest.mock('../src/models/DatabaseAbstract', () => {
    return jest.fn().mockImplementation(() => {
        return {
            getData: () => {
                return mockDatabaseReturn;
            }
        }
    });
});

beforeEach(() => {
    DatabaseAbstract.mockClear();
});

describe('Calculate routes and prices', () => {
    
    let from = 'GRU';
    let to   = 'CDG';

    let CalculateInstance = new Calculate(from, to);
    
    const allRoutes = [
        { route: 'GRU,BRC,SCL,ORL,CDG', price: 40 },
        { route: 'GRU,SCL,ORL,CDG', price: 45 },
        { route: 'GRU,ORL,CDG', price: 61 },
        { route: 'GRU,CDG', price: 75 }
    ]

    it('Calculating all possibilities', async () => {
        let data = await CalculateInstance.genereteBestsRoutes();

        expect(data).toStrictEqual(allRoutes);
    })

    it('Return min price', async () => {
        let data = await CalculateInstance.getBestPrice(allRoutes);

        let expectReturn = { route: 'GRU,BRC,SCL,ORL,CDG', price: 40 };
            
        expect(data).toStrictEqual(expectReturn);
    })

    it('Empth route', async () => {
        let from = 'GRU';
        let to   = 'RJ';

        let CalculateInstance = new Calculate(from, to);

        let data = await CalculateInstance.genereteBestsRoutes();

        expect(data).toStrictEqual([]);
    })
})